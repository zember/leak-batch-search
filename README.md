A script that looks for multiple patterns in huge data leaks at once and then
splits them into files accordingly, one output file per pattern. 

It supports ZSTD.

Usage: 

```
./leak-batch-search <searchdir>
```

 * searchdir contains file `patterns` with patterns to search for and output

Syntax of file `patterns`:

 * fixed string patterns (one per line, not regex's)
 * lines starting with '#' (e.g. '# myfamily') mean that following patterns should 
                be groupped together into a separate directory (in this case, 'myfamily/')

Example:

```
./leak-batch-search.sh example-search
[*] Storing results in example-search/output1/
[*] These are the patterns from patterns to look for...
myextra123456
# mygamincompetitor
gamefreak100001@
supergamer26
# xboxpeople
xbox360reach@
xboxjeff@
[*] Performing big search with all patterns...
/mnt/leak/leak-batch-search/small-leak-DB-for-testing/0.txt.zst:myextra123456@gmail.com:20004709
/mnt/leak/leak-batch-search/small-leak-DB-for-testing/0.txt.zst:supergamer26@yahoo.com:tigercat1
/mnt/leak/leak-batch-search/small-leak-DB-for-testing/0.txt.zst:gamefreak100001@aol.com:nintendo
/mnt/leak/leak-batch-search/small-leak-DB-for-testing/0.txt.zst:xbox360reach@gmail.com:95694325
/mnt/leak/leak-batch-search/small-leak-DB-for-testing/0.txt.zst:xboxjeff@comcast.net:udt8ruycn
[*] Big search completed. Sorting results...
[*] Done. Results are stored in example-search/output1/.
```

Now the first pattern, 'myextra123456' is stored in a separate file.

The following two patterns are in a group "mygamincompetitor", thus the results
will be in files under a subdirectory:

```
output1/mygamincompetitor/supergamer26
output1/mygamincompetitor/gamefreak100001@
```

By looking at the sizes of the files, it is easy to see which contain any data.