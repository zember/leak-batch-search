PATTERNFILENAME=patterns
usage() {
	cat <<EOU
A script that looks for multiple patterns in huge data leaks at once and then splits them
into files accordingly, one output file per pattern. It supports ZSTD.

Usage: $0 <searchdir>
	searchdir contains file '$PATTERNFILENAME' with patterns to search for and output

Syntax of file '$PATTERNFILENAME':
	fixed string patterns (one per line, not regex's)
	lines starting with '#' (e.g. '# myfamily') mean that following patterns should 
		be groupped together into a separate directory (in this case, 'myfamily/')
EOU
}

LEAKDIR=`realpath ./small-leak-DB-for-testing`

if [ ! -d "$LEAKDIR" ]; then
	echo "[*] Directory $LEAKDIR not found."
	echo "[*] Configure variable LEAKDIR in the script."
	exit
fi

SEARCHDIR="$1"
if [ -z "$SEARCHDIR" ]; then
	usage
	exit
fi
if [ ! -d "$SEARCHDIR" ]; then
	echo "[*] Directory $SEARCHDIR not found."
	usage
	exit
fi
cd "$SEARCHDIR"

# create output directory
if [ -d "output1" ]; then
	OUTDIR=output$(expr `ls -d output[0-9]* -v | sed -n '$s/output//p'` + 1)
	echo "[*] Output directories from previous searches found. Storing results in $SEARCHDIR/$OUTDIR/."
else
	OUTDIR=output1
	echo "[*] Storing results in $SEARCHDIR/$OUTDIR/"
fi
PARENTOUTDIR="$SEARCHDIR/$OUTDIR"
mkdir $OUTDIR

ALLRESULTS=$OUTDIR/allresults

echo "[*] These are the patterns from $PATTERNFILENAME to look for..."
cat "$PATTERNFILENAME" | sed '/^$/d' 

echo "[*] Performing big search with all patterns..."

find $LEAKDIR -print0 | xargs --null zstdgrep --fixed-strings $"`cat $PATTERNFILENAME | sed '/^#/d' | sed '/^$/d' `" | tee $ALLRESULTS

echo "[*] Big search completed. Sorting results..."
while read LINE; do
	if echo "$LINE" | grep -q '^#'; then
		PROJECT=`echo "$LINE" | sed 's/^#\s*//'`
		if [ -n "$OUTDIRBACKUP" ]; then
			OUTDIR="$OUTDIRBACKUP"
		fi
		OUTDIRBACKUP="$OUTDIR"
		OUTDIR="$OUTDIR/$PROJECT"
		mkdir "$OUTDIR"
	else
		if [ -n "$LINE" ]; then
			PATTERN="$LINE"
			grep -F "$PATTERN" "$ALLRESULTS" > "$OUTDIR/$PATTERN"
		fi
	fi
done < "$PATTERNFILENAME"

echo "[*] Done. Results are stored in $PARENTOUTDIR/."
